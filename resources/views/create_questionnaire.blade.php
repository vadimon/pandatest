@extends('layouts.main')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('questionnaire.index')}}">Назад до списку</a></li>
            <li class="breadcrumb-item">Створення нової анкети</li>
        </ol>
    </nav>

    <div class="container">
        <div class="card">
            <div class="card-title text-center"><span class="text-danger">*</span> Поля обовязкові для заповннення</div>
            <div class="card-body">
                <form method="POST" action="{{route('questionnaire.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label><span class="text-danger">*</span> Ім'я</label>
                        <input name="name" type="text" class="form-control col-4 @error('name') is-invalid @enderror" value="{{old('name')}}" required>
                        <small class="form-text text-muted">Введіть ваше ім'я</small>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Зображення</label>
                        <input name="file[]" type="file" class="form-control-file @error('file') is-invalid @enderror" multiple>

                        @error('file.*')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-outline-primary">Зберегти</button>
                </form>
            </div>
        </div>
    </div>

@endsection