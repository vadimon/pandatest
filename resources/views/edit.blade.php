@extends('layouts.main')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('questionnaire.index')}}">Назад до списку</a></li>
            <li class="breadcrumb-item">Редагування анкети</li>
        </ol>
    </nav>

    <div class="container">
        <div class="card">
            <div class="card-title text-center"><span class="text-danger">*</span> Поля обовязкові для заповннення</div>
            <div class="card-body">
                <form method="POST" action="{{route('questionnaire.update',$questionnaire->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    @if(session('success'))
                        <div class="row justify-content-center">
                            <div class="col-md-3">
                                <div class="alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">x</span>
                                    </button>
                                    {{ session()->get('success') }}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label><span class="text-danger">*</span> Ім'я</label>
                        <input name="name" type="text" class="form-control col-4 @error('name') is-invalid @enderror" value="{{old('name') ?? $questionnaire->name}}" required>
                        <small class="form-text text-muted">Введіть ваше ім'я</small>
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label><span class="text-danger">*</span> Зображення</label>
                        @foreach(json_decode($questionnaire->file, true) ?? [] as $key => $path)
                            <div>
                                <img src="{{asset('/storage/'.$path)}}" class="img-thumbnail" width="100px" alt="..."> <button type="button" class="btn btn-sm btn-danger" data-info="{{$path}}" data-id="{{$questionnaire->id}}" onclick="deleteImage(this)">--</button>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>Додати зображення</label>
                        <input name="file[]" type="file" class="form-control-file @error('file') is-invalid @enderror" multiple>

                        @error('file.*')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-outline-primary">Зберегти</button>
                </form>
            </div>
        </div>
    </div>

@endsection