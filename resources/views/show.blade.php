@extends('layouts.main')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('questionnaire.index')}}">Назад до списку</a></li>
            <li class="breadcrumb-item">Перегляд анкети</li>
        </ol>
    </nav>
    @if(session('success'))
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    {{ session()->get('success') }}
                </div>
            </div>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="card-title"><span class="font-weight-bold">{{$questionnaire->name}}</span></div>
        </div>
        <div class="card-body">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @for($j = 0;$j < $count; $j++)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$j}}" @if($j == 0) class="active"@endif></li>
                    @endfor
                </ol>
                <div class="carousel-inner">
                    @foreach(json_decode($questionnaire->file, true) ?? [] as $key => $path)
                        <div class="carousel-item @if($key == 0) active  @endif">
                            <img src="{{asset('/storage/'.$path)}}" class="d-block w-100" alt="...">
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
@endsection