@extends('layouts.main')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title"><span class="font-weight-bold">Тестове завданння для PandaTeam</span></div>
        </div>
        <div class="card-body">Для перегляду завдання натисніть сюда ==> <a href="{{route('questionnaire.index')}}">Переглянути</a></div>
    </div>
@endsection