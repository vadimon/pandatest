@extends('layouts.main')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title"><span class="font-weight-bold">Всі Анкети</span></div>
        </div>
        @if(session('success'))
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        {{ session()->get('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="card-body">
            <a href="{{route('questionnaire.create')}}" class="btn btn-outline-info mb-2">Створити</a>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ім'я</th>
                    <th scope="col">Зображення</th>
                    <th scope="col">Редагувати</th>
                    <th scope="col">Видалити</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questionnaires as $key => $questionnaire)
                <tr>
                    <th scope="row">{{$key + 1}}</th>
                    <td><a href="{{route('questionnaire.show',$questionnaire->id)}}">{{{$questionnaire->name}}}</a></td>
                    <td>
                        @if($questionnaire->file)
                            <img src="{{asset('/storage/'.json_decode($questionnaire->file, true)['0'])}}" alt="" class="img-thumbnail" width="100px" >
                        @else
                            <img src="http://dummyimage.com/150x60/99cccc.jpg/0b0b0b&text=The+image!" />
                        @endif

                    </td>
                    <td><a href="{{route('questionnaire.edit',$questionnaire->id)}}" class="btn btn-sm btn-outline-primary">edit</a></td>
                    <td>
                        <form action="{{route('questionnaire.destroy',$questionnaire->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger">-</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @if($questionnaires->total() > $questionnaires->count())
                <br>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                {{$questionnaires->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection