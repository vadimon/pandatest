<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'file'
    ];
}
