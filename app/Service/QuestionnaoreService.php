<?php


namespace App\Service;


use App\Models\Questionnaire;
use Storage;

class QuestionnaoreService
{
    /**
     * @param $data
     * @return bool
     */
    public function deleteImage($data):bool
    {
        $questionnaire = Questionnaire::find($data['id']);
        $image = $data['image'];
        $arrayImage = json_decode($questionnaire->file, true);
        foreach ( $arrayImage as $key => $file) {
            if ($file === $image){
                if (Storage::exists('public/'.$image)){
                    Storage::delete('public/'.$image);
                    unset($arrayImage[$key]);
                } else {
                    $error = 'Файл не знайдено';
                }
            }
        }
        if(!isset($error)){
            if (!empty($arrayImage)){
                $questionnaire->file = json_encode($arrayImage);
            } else {
                $questionnaire->file = null;
            }
            $questionnaire->save();
            return true;
        }
            return false;
    }
}