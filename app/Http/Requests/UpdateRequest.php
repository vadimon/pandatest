<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/[a-zA-Zя-яА-я]/','min:3', 'max:30'],
            'file' => ['nullable', 'array'],
            'file.*' => ['mimes:jpg,png,jpeg', 'file', 'max:3072']
        ];
    }
    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'name' => 'Ім\'я',
            'file' => 'Зображення'
        ];
    }

    /**
     * @return array|void
     */
    public function passedValidation()
    {
        $data = $this->validated();
        if (isset($data['file'])){
            $path = [];
            foreach ($data['file'] as $file) {
                $path[] = $file->store($data['name'],'public');
            }

            $data['file'] = $path;
        }
        return $data;

    }
}
