<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateRequest;
use App\Models\Questionnaire;
use App\Service\QuestionnaoreService;
use Illuminate\Http\Request;

class MainController extends Controller
{

    protected $service;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        $this->service = app(QuestionnaoreService::class);
    }
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $questionnaires = Questionnaire::paginate(5);

        return view('all_questionnaire', compact('questionnaires'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return  view('create_questionnaire');
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $data = $request->passedValidation();

        Questionnaire::create($data);

        return redirect()->route('questionnaire.index')->with(['success' => 'Анкета успішно збережена']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::find($id);
        $count = count(json_decode($questionnaire->file, true) ?? []);

        return view('show', compact('questionnaire', 'count'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $questionnaire = Questionnaire::find($id);

        return view('edit', compact('questionnaire'));
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->passedValidation();
        $questionnaire = Questionnaire::find($id);
        $file = json_decode($questionnaire->file,true);
        $updateFile = array_merge($file,$data['file']);
        $questionnaire->name = $data['name'];
        $questionnaire->file = json_encode($updateFile);
        $questionnaire->save();

        return back()->with(['success' => 'Дані оновленні']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::find($id)->delete();
        if ($questionnaire){
            return back()->with(['success' => 'Успішно видалено']);
        }
        return back()->withErrors(['error' => 'Помилка видалення']);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        if ($request->ajax()){
            $data  = $request->request->get('data');
             $result =  $this->service->deleteImage($data);
             if ($result){
                 return response('success',200);
             } else {
                 return response('error',500);
             }
        }
    }
}
