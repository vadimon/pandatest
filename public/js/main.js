function deleteImage(imagePath){
    let data = {
        'image': $(imagePath).data('info'),
        'id': $(imagePath).data('id')
    }
    $.ajax({
        url: `${document.location.origin}/deleteImage`,
        method: "GET",
        context: imagePath,
        data: {'data' : data},
        success: function (data){
            console.log(data)
            $(imagePath).parent().remove();
            alert(data);
        },
        error: function (data){
            console.log(data);
            alert(data);
        }
    });
}